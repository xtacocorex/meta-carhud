DESCRIPTION = "Arduino like Wiring library for Raspberry Pi"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING.LESSER;md5=e6a600fd5e1d9cbde2d983680233ad02"

PR = "r1"

COMPATIBLE_MACHINE = "raspberrypi carhud"

SRCREV = "ddf1b3ffaad29165c3e260eb00e8dd42900ae8ae"
SRC_URI = "git://github.com/WiringPi/WiringPi.git;protocol=git;branch=master \ 
           file://build.patch "
S = "${WORKDIR}/git"

inherit autotools

do_compile() {
    ${S}/build compile
}

do_install() {
    ${S}/build install
}

