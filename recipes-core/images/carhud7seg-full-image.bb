# Base this image on core-image-minimal
include recipes-core/images/core-image-minimal.bb

IMAGE_ROOTFS_SIZE = "8192"

# Include modules in rootfs
IMAGE_INSTALL += " \
	kernel-modules \
	"

IMAGE_FEATURES += "ssh-server-dropbear splash"
# read-only-rootfs"

# SDL
#IMAGE_INSTALL += "libsdl jpeg libsdl-dev libsdl-ttf libsdl-ttf-dev libsdl-mixer libsdl-mixer-dev libsdl-image libsdl-image-dev"

# PYTHON 2.7
IMAGE_INSTALL += "libpython2 \
                  python-audio \
                  python-bsddb \
                  python-codecs \
                  python-compile \
                  python-compiler \
                  python-compression \
                  python-core \
                  python-crypt \
                  python-ctypes \
                  python-curses \
                  python-datetime \
                  python-db \
                  python-debugger \
                  python-dev \
                  python-difflib \
                  python-distutils-staticdev \
                  python-distutils \
                  python-doctest \
                  python-elementtree \
                  python-email \
                  python-fcntl \
                  python-gdbm \
                  python-hotshot \
                  python-html \
                  python-image \
                  python-io \
                  python-json \
                  python-lang \
                  python-logging \
                  python-mailbox \
                  python-math \
                  python-mime \
                  python-mmap \
                  python-multiprocessing \
                  python-netclient \
                  python-netserver \
                  python-numbers \
                  python-pickle \
                  python-pkgutil \
                  python-pprint \
                  python-profile \
                  python-pydoc \
                  python-re \
                  python-readline \
                  python-resource \
                  python-robotparser \
                  python-shell \
                  python-smtpd \
                  python-stringold \
                  python-subprocess \
                  python-syslog \
                  python-terminal \
                  python-tests \
                  python-textutils \
                  python-threading \
                  python-tkinter \
                  python-unittest \
                  python-unixadmin \
                  python-xml \
                  python-xmlrpc \
                  python-zlib \
                  python-modules \
                  python-misc \
                  python-man "

# RASPBERRY PI GPIO + I2C TOOLS + PYTHON-SMBUS
IMAGE_INSTALL += "rpi-gpio rpio python-smbus i2c-tools i2c-tools-dev swig wiringpi2-python"

# ADAFRUIT RASPBERRY PI PYTHON LIBRARIES
IMAGE_INSTALL += "adafruit-python-gpio adafruit-python-ledbackpack"

# CAR HUD NEEDED PYTHON LIBRARIES
IMAGE_INSTALL += "python-pyserial python-jsocket"

# PYELM327
IMAGE_INSTALL += "pyelm327"

# PYGAME
#IMAGE_INSTALL += "python-pygame"

# GPSD - FROM META-OPENEMBEDDED
IMAGE_INSTALL += "gpsd libgps python-pygps gpsd-conf gpsd-udev gpsd-gpsctl gps-utils"

# CARHUD CODE - NO INIT SCRIPTS FOR DEBUG VERSION
IMAGE_INSTALL += "pycarhud carhud-extras carhud-config carhudgui7seg-startup-init carhudgui7seg-init"
