DESCRIPTION = "Arduino like Wiring library for Raspberry Pi with Python Bindings"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://README;md5=7f4b11d304341f1d27d0a900182613b1"

PR = "r1"

#DEPENDS = "wiringpi"

COMPATIBLE_MACHINE = "(raspberrypi|carhud)"

SRCREV = "97a4f5131ee0101e5d92407c6f5974b8e8dcf0ce"
SRC_URI = "git://github.com/WiringPi/WiringPi2-Python.git;protocol=git;branch=master"
S = "${WORKDIR}/git"

inherit setuptools


#do_compile() {
#    ${S}/build.sh
#}
